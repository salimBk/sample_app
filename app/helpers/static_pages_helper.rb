module StaticPagesHelper

	def full_title(title)
		baseTitle = "Ruby on Rails Tutorial Sample App"
		if title == "" 
			baseTitle
		else
			"#{baseTitle} | #{title}"
		end
	end

end
